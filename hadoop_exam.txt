1. Spark 2.x Scala Certification Videos 1 :  Introduction to Databricks Spark 2.x Scala Certification : Available (Length 14 Minutes)
In this video we are giving  brief explanation about Databricks new Saprk 2.x Developer certification. 
What all are included in syllabus.
How to prepare for this certification?
Why this certification become popular in 2018 and will remain popular at least for 2019.
2. Spark 2.x Scala Certification Videos 2 :  SparkSession objects : Available (Length 19 Minutes)
SparkSession object and SparkContext object use in Spark 2.x. 
Loading csv files
Basics of DataFrame and Datasets
Finding correct schema for given Datasets using StructType and StructField.
3. Spark 2.x Scala Certification Videos 3 :  DataFrame, RDDs, GroupBy  : Available (Length 16 Minutes)
Select correct code for DataFrame related stuff. 
Applying GroupBy functions on the data to select specific record.
Save data back to file system as csv only selected columns.
Getting original RDD data from the DataFrame and what will be datatypes.
4. Spark 2.x Scala Certification Videos 4 :  Datasets, RDD and SparkSQL : Available (Length 10 Minutes)
Getting original RDD from Dataset and what will be its datatypes. 
SparkSQL on the Dataset object.
Dataset API and Dataset as a temporary view and getting same result using API and SparkSQL?
5. Spark 2.x Scala Certification Videos 5 : GraphFrames, vertices, edges and out Degrees : Available (Length 10 Minutes)
Creating GraphFrame using DataFrame objects. 
GraphFrame vertices and edges DataFrames.
Applying filter transformations on the GraphFrame objects.
GraphFrame vertices out Degree.
6. Spark 2.x Scala Certification Videos 6 :  GraphFrame and Motif expressions : Available (Length 10 Minutes)
Select correct motif expression for getting desired result form GraphFrame object. 
Applying filter transformation on Motif expressions.
Applying groupBy, order by functions.
7. Spark 2.x Scala Certification Videos 7 :  GraphFrame and social network use case : Available (Length 9 Minutes)
Using GraphFrame to solve social network problems. 
Filtering and naming the columns in case of Motifs.
DataFrame relation join operation.
8. Spark 2.x Scala Certification Videos 8 : Spark Jobs, Stages , Tasks and Spark web UI : Available (Length 15 Minutes)
What is the difference between Spark jobs, Stages and tasks. 
What is the relation of Tasks and available cores in Spark cluster.
How to calculate the jobs and stages for given code?
Relation between Tasks and Data Partitioning.
Relation between data shuffling and stages.
Going through Spark web UI to check Jobs, Stages and Tasks.
9. Spark 2.x Scala Certification Videos 9 :  Spark RDD, Accumulators calculations : Available (Length 8 Minutes)
Applying and using accumulators in a transformation. 
Impact of node failure on Accumulators values.
Various failure use cases for accumulators calculation in transformations.
10. Spark 2.x Scala Certification Videos 10 :  RDDs and Data append : Available (Length 12 Minutes)
How to append data from more than one RDD. 
Applying and union function and not to confuse with reduce, join operations.
11. Spark 2.x Scala Certification Videos 11 :  Broadcast variables  : Available (Length 10 Minutes)
How to create a broadcast variables. 
How to de-serialize broadcast variables and use them as a collection.
PairRDD and reducebyKey function?
12. Spark 2.x Scala Certification Videos 12 :  PairRDD and Combiner : Available (Length 12 Minutes)
Understand use of combiner. 
Local combiner and global combiner.
Using combineByKey function on PairRDD.
13. Spark 2.x Scala Certification Videos 13 :  PairRDD and reduceByKey : Available (Length 11 Minutes)
Select the correct program which generate the desired result from given option.
reduceByKey function will be used and explained.
14. Spark 2.x Scala Certification Videos 14 :  Data caching : Available (Length 4 Minutes)
How to cache the data using RDD. 
What would happen when you cache the data.
Caching data which can only fit into memory.
15. Spark 2.x Scala Certification Videos 15 :  Various types of relational data joining : Available (Length 6 Minutes)
select correct join operations from leftOuterJoin, rightOuterJoin, fullouterJoin, Join (Inner join)
